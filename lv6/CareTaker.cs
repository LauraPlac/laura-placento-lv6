﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lv6
{
    //Z3.

    class CareTaker
    {
        private List<Memento> mementos;

        public CareTaker() 
        {
            this.mementos = new List<Memento>();
        }

        public void AddMemento(Memento memento)
        {
            this.mementos.Add(memento);
        }

        public void RemoveLastMemento()
        {
            if(mementos.Count == 0)
            {
                throw new IndexOutOfRangeException("Out of range");
            }

            this.mementos.RemoveAt(mementos.Count - 1);
        }

        public void RemoveMemento(Memento memento)
        {
            this.mementos.Remove(memento);
        }

        public Memento GetLastMemento()
        {
            return this.mementos[mementos.Count - 1];
        }

        

    }

}
