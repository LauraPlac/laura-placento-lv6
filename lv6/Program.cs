﻿using System;
using System.Security.Cryptography;

namespace lv6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Z1.
            Notebook notebook = new Notebook();
            Note note1 = new Note("Note1", "This is note 1");
            Note note2 = new Note("Note2", "This is note 2");
            Note note3 = new Note("Note3", "This is note 3");

            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);

            Iterator iterator = (Iterator)notebook.GetIterator();

            for(int i = 0; i < notebook.Count; i++)
            {
                iterator.Current.Show();
                iterator.Next();
            }

            Console.WriteLine("\n");

            //Z2.
            Box box = new Box();
            Product product1 = new Product("Mlijeko", 4);
            Product product2 = new Product("Sir", 7);
            Product product3 = new Product("Cokolada", 10);

            box.AddProduct(product1);
            box.AddProduct(product2);
            box.AddProduct(product3);

            BoxIterator boxIterator = (BoxIterator)box.GetIterator();

            for (int i = 0; i < box.Count; i++)
            {
                Console.WriteLine(boxIterator.Current);
                boxIterator.Next();
            }

            Console.WriteLine("\n");

            //Z3.

            CareTaker careTaker = new CareTaker();
            ToDoItem toDoItem = new ToDoItem("Learn","Learn Math", DateTime.Now);
            Memento memento = toDoItem.StoreState();
            careTaker.AddMemento(memento);
            toDoItem = new ToDoItem("Clean", "Clean house", DateTime.Now);


            try
            {
                Console.WriteLine(toDoItem);
                toDoItem.RestoreState(careTaker.GetLastMemento());
                Console.WriteLine(toDoItem);
                careTaker.RemoveLastMemento();
                careTaker.RemoveLastMemento();

            }
            catch(IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("\n");

            //Z4.
            BankAccount bankAccount = new BankAccount("Laura", "Osijek", 1000);
            BankAccountMemento bankAccountMemento = bankAccount.StoreState();
            Console.WriteLine("Balance: " + bankAccount.Balance);
            bankAccount.UpdateBalance(-500);
            Console.WriteLine("Balance: " + bankAccount.Balance);
            bankAccount.RestoreState(bankAccountMemento);
            Console.WriteLine("Balance: " + bankAccount.Balance);
            
            Console.WriteLine("\n");

            //Z5.
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            logger.SetNextLogger(fileLogger);

            logger.Log("INFO", MessageType.INFO);
            logger.Log("ERROR", MessageType.ERROR);
            logger.Log("WARNING", MessageType.WARNING);

        }
    }
}
